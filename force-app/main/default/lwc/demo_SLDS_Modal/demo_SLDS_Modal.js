import { LightningElement } from 'lwc';

export default class Demo_SLDS_Modal extends LightningElement {
    //add eventListner to handle keystrokes
    connectedCallback() {
        document.addEventListener("keydown", this.handleEscapeKey.bind(this));
    }

    handleEscapeKey(event){
        //check if the escape key was pressed and if the modal is open
        if(event.key === 'Escape' && this.isModalOpen){
            //close the modal
            this.closeModal();
        }
    }
}